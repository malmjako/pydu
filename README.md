pydu -- simple program for visually displaying disk usage outputs from du.

Author: Jakob Malm <jakob.malm@gmail.com>
License: CC-SA

# Description #

I created this little program to be able to run a disk usage scan (du --bytes /)
and later look at the result visually. Disk usage is displayed using matplotlib
in a very crude way -- there is room for great improvement if anyone is interested.
Ideally, the disk usage would be presented in  a way similar to how baobab or
WinDirStat displays it.

# Usage #


```
#!sh

du --bytes / > du.txt
python pydu.py du.txt
```
