#!/usr/bin/env python
# -*- coding: utf-8 -*- 

"""
Parse output from `du` and plot the result in a clickable bar graph.

"""

__all__ = ['SizedDir', 'parse_du']

import os
from datetime import date
import matplotlib.pyplot as plt

MAX_CATALOGS = 16


def sizeof_fmt(num):
    """
    From http://stackoverflow.com/a/1094933.

    """
    if num < 1024 and num > -1024:
        return "{0} bytes".format(num)

    num /= 1024.0
    
    for x in ['KB','MB','GB']:
        if num < 1024.0 and num > -1024.0:
            return "%3.1f%s" % (num, x)
        num /= 1024.0
    return "%3.1f %s" % (num, 'TB')


class SizedDir(object):
    def __init__(self, path, size, parent=None):
        self.path = path
        self.size = size
        self.children = set()
        if parent is None:
            self.percent = 100.
        elif parent.size == 0:
            self.percent = 0.
        else:
            self.percent = 100. * size / parent.size
    
    def add(self, path, size):
        """
        Add a new :class:`SizedDir` at *path* in the directory tree with *size*.
        
        """
        childpath, descendants = self.__separate(path)
        if descendants:
            child = self.get_child(childpath)
            if child is not None:
                child.add(path, size)
            else:
                raise ValueError("No child '%s' in parent '%s'" %
                                 (os.path.basename(childpath), self.path))
        else:
            self.children.add(SizedDir(path, size, self))
    
    def get_child(self, path):
        """
        Get child directory at *path*.
        
        """
        for child in self.children:
            if child.path == path:
                return child

    def plot(self, date, filename=None):
        """
        Plot a (clickable) bar graph of this directory and display it. If
        *filename* is not None, write plot to file instead.
        
        """
        fig = plt.figure()
        ax = fig.add_subplot(111)
        
        bars_subdirs = {}
        ix = 0
        for subdir in sorted(self.children, reverse=True):
            bar_obj = ax.barh(ix, subdir.percent, fc='yellow', ec='none', picker=5)
            bars_subdirs[bar_obj[0]] = subdir
            name = os.path.basename(subdir.path)
            ax.annotate(" %s (%s (%.1f %%)" % (name, sizeof_fmt(subdir.size), subdir.percent),
                     (0, ix + .5), size=10)
            if ix >= MAX_CATALOGS - 1:
                break
            ix += 1
        
        ax.set_xlabel("% of total usage")
        
        title_string = "%s usage %s" % (self.path, date.strftime("%F"))
        if len(self.children) > MAX_CATALOGS:
            title_string += " (top %d catalogs)" % MAX_CATALOGS
        ax.set_title(title_string)
        ax.invert_yaxis()
        ax.set_yticks([])
        
        def onclick(event):
            from matplotlib.patches import Rectangle
            if isinstance(event.artist, Rectangle):
                dir = bars_subdirs[event.artist]
                if len(dir.children) > 0:
                    dir.plot(date)
        
        fig.canvas.mpl_connect('pick_event', onclick)
        
        if filename is None:
            plt.show()
        else:
            fig.savefig(filename)
    
    def __separate(self, path):
        dirs = path.split(os.path.sep)
        parent_dirs = self.path.rstrip('/').split(os.path.sep)
        childpath = os.path.join(self.path, dirs[len(parent_dirs)])
        if len(dirs) == len(parent_dirs) + 1:
            return childpath, False
        return childpath, True

    def __hash__(self):
        return hash(self.path)
    
    def __eq__(self, other):
        if hasattr(object, 'size'):
            return self.size == other.size
        return self.size == other
    
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __lt__(self, other):
        if hasattr(object, 'size'):
            return self.size < other.size
        return self.size < other
    
    def __gt__(self, other):
        if hasattr(object, 'size'):
            return self.size > other.size
        return self.size > other
    
    def __le__(self, other):
        return not self.__gt__(other)
    
    def __ge__(self, other):
        return not self.__lt__(other)
    
    def __str__(self):
        return "<SizedDir: '%s' %d (%.1f %%)>" % (self.path, self.size, self.percent)
    
    def __repr__(self):
        return self.__str__()


def parse_du(lines):
    """
    Build a :class:`SizedDir` tree based on output from ``du`` in *lines* .
    
    Parameters
    ----------
    lines : list of strings
        List of strings, as output from ``du``.
    
    Returns
    -------
    root : :class:`SizedDir`
        The root :class:`SizedDir` in the directory tree, with descendants.
    
    Examples
    --------
    >>> lines = '''50\t/base/path/sub1/sub1.1
    75\t/base/path/sub/1/sub1.2
    400\t/base/path/sub2
    500\t/base/path'''
    >>> parse_du(lines)
    <SizedDir: '/base/path' 500 (100.0 %)>
    
    """
    root = None
    for l in reversed(lines):
        size = int(l.split('\t')[0])
        path = l.rstrip('\n').split('\t')[-1]
        # Convert to unicode if it's not already a unicode. (Only relevant for Python2.)
        try:
            path = path.decode('utf8')
        except AttributeError:
            pass
        if root is None:
            if not path == '/':
                path = path.rstrip(os.path.sep) # discard trailing '/'
            root = SizedDir(path, size)
        else:
            root.add(path, size)
    
    return root


def run_cli():
    from optparse import OptionParser
    
    parser = OptionParser("%prog [options] FILE\n"
                          "Parse output from `du` and plot in (clickable) bar graphs.\n")
    parser.add_option('-f', '--file', type='string',
                      help="Save plot to FILE instead of showing it.")
    (options, arguments) = parser.parse_args()
    
    du_log_path = arguments[0]
    du_log_date = date.fromtimestamp(os.path.getctime(du_log_path))
    
    du_log_file = open(du_log_path)
    lines = du_log_file.readlines()
    du_log_file.close()
    
    tree = parse_du(lines)
    
    tree.plot(du_log_date, options.file)

if __name__ == "__main__":
    run_cli()
