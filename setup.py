#!/usr/bin/env python

try:
    from setuptools import setup
    #from setuptools.extension import Extension
except ImportError:
    from distutils.core import setup

from version import get_git_version

setup(name='pydu',
    description="Parse output from `du` and display in (clickable) bar graph",
    author="Jakob Malm",
    author_email="jakob.malm@smhi.se",
    version=get_git_version(),
    py_modules=['pydu', 'pyduplot'],
    test_suite="nose.collector",
    tests_require="nose",
    include_package_data=True,
    zip_safe=False,
    entry_points = {'console_scripts': ["pydu = pydu:run_cli"]}
    )