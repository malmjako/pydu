"""
Plot usage statistics

"""
from __future__ import with_statement
import sys

def parse_filename(filename):
    from datetime import datetime
    return datetime.strptime(filename, "usage_%Y-%m-%d_%H:%M:%S.txt")

def last_row(filename):
    with open(filename, 'r') as f:
        return f.readlines()[-1]

def total_size(filename):
    """Return total size (size reported on last row of *filename*)."""
    import re
    return int(re.match("([0-9]*)\t.*", last_row(filename)).groups()[0])

def plot(times, sizes):
    import matplotlib.pyplot as plt
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(times, sizes, 'o--')
    ax.set_ylim(bottom=0)
    fig.autofmt_xdate()
    
    return fig

if __name__ == '__main__':
    files = sys.argv[1:]
    times = [parse_filename(f) for f in files]
    sizes = [total_size(f) for f in files]
    plot(times, sizes).savefig('usage-plot.png')
