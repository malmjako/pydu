"""
Unit tests for pydu.py

"""

import unittest
from random import randint

# Import public API
from pydu import *


class TestPydu(unittest.TestCase):
    def test_sizeddir(self):
        root_path = '/path/to/root'
        root_size = randint(0, 1024**3)
        
        child_params = []
        for i in range(randint(3, 10)):
            child_params.append({'path': root_path + '/dir%d' % i, 'size': randint(0, 1024**3)})
        
        root = SizedDir(root_path, root_size)
        self.assertEqual(root.path, root_path)
        self.assertEqual(root.size, root_size)
        
        for c in child_params:
            root.add(c['path'], c['size'])
        
        self.assertEqual(len(root.children), len(child_params))
        
        for c in child_params:
            child = root.get_child(c['path'])
            self.assertEqual(child.path, c['path'])
            self.assertEqual(child.size, c['size'])
        
        # Check sorting
        sizes = []
        for child in root.children:
            sizes.append((child.size, child))
        sizes.sort()
        self.assertEqual([child for size, child in sizes], sorted(root.children))
        
    
    def test_parse_du(self):
        lines = ["50\t/base/path/sub1/sub1.1\n",
                 "75\t/base/path/sub1/sub1.2\n",
                 "100\t/base/path/sub1\n",
                 "400\t/base/path/sub2\n",
                 "500\t/base/path\n"]
        root = parse_du(lines)
        
        self.assertEqual(root.path, "/base/path")
        self.assertEqual(root.size, 500)
        self.assertEqual(len(root.children), 2)
        
        sub1 = root.get_child("/base/path/sub1")
        self.assertEqual(sub1.size, 100)
        self.assertEqual(len(sub1.children), 2)
        
        sub1_2 = sub1.get_child("/base/path/sub1/sub1.2")
        self.assertEqual(sub1_2.size, 75)
        self.assertEqual(len(sub1_2.children), 0)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()